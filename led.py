#!/usr/bin/env python

import argparse
import bluepy
import colorsys
import subprocess
import time
import typing

def uint8_t(value: int):
  return max(0, min(value, 255))

class LEDController:

  def __init__(self, address, address_type, service_uuid, characteristic_uuid,
               hsv: typing.Tuple[float] = (0.0, 0.0, 0.0),
               hsv_min: typing.Tuple[float] = (0.0, 0.0, 0.0),
               hsv_max: typing.Tuple[float] = (1.0, 1.0, 1.0),
               hsv_period: typing.Tuple[float] = (0.0, 0.0, 0.0),
               hsv_offset: typing.Tuple[float] = (0.0, 0.0, 0.0),
               hsv_reverse: typing.Tuple[bool] = (False, True, True)):
    self.address = address
    self.address_type = address_type
    self.service_uuid = service_uuid
    self.characteristic_uuid = characteristic_uuid
    self.hsv = list(hsv)
    self.hsv_min = hsv_min
    self.hsv_max = hsv_max
    self.hsv_period = hsv_period
    self.hsv_offset = hsv_offset
    self.hsv_reverse = hsv_reverse

    self.led = None
    self.service = None
    self.characteristic = None

  def disconnect(self):
    self.led = None
    self.service = None
    self.characteristic = None

  def connect(self):
    self.led = bluepy.btle.Peripheral(self.address, addrType=self.address_type)
    self.service = self.led.getServiceByUUID(self.service_uuid)
    self.characteristic = self.service.getCharacteristics(forUUID=self.characteristic_uuid)[0]
    self.on()

  def is_connected(self):
    return self.led is not None and self.service is not None and self.characteristic is not None

  def write(self, data):
    self.characteristic.write(bytes(data))

  def off(self):
    self.write((0xcc, 0x24, 0x33))

  def on(self):
    self.write((0xcc, 0x23, 0x33))

  def set_led(self, red: int = 0, green: int = 0, blue: int = 0,
              white: int = 0, colour: bool = True):
    self.write((
      0x56, # ?
      uint8_t(red), # Red
      uint8_t(green), # Green
      uint8_t(blue), # Blue
      uint8_t(white), # White
      0xf0 if colour else 0x0f, # ?
      0xaa, # ?
    ))

  def set_colour(self, red: int = 0, green: int = 0, blue: int = 0):
    self.set_led(red=red, green=green, blue=blue, white=0, colour=True)

  def set_white(self, white: int = 0):
    self.set_led(red=0, green=0, blue=0, white=white, colour=False)

  def update(self):
    for hsv_index in (0, 1, 2):
      if self.hsv_period[hsv_index] > 0:
        period_progress = ((time.time() + self.hsv_offset[hsv_index]) % self.hsv_period[hsv_index]) / self.hsv_period[hsv_index]
        if self.hsv_reverse[hsv_index]:
          period_progress = 1.0 - 2.0 * abs(period_progress - 0.5)
        self.hsv[hsv_index] = self.hsv_min[hsv_index] + period_progress * (self.hsv_max[hsv_index] - self.hsv_min[hsv_index])

    red, green, blue = colorsys.hsv_to_rgb(*self.hsv)
    self.set_colour(red=int(255 * red), green=int(255 * green), blue=int(255 * blue))

    # print("hsv", list(int(255 * hsv_value) for hsv_value in self.hsv))
    # print("rgb", [int(255 * red), int(255 * green), int(255 * blue)])

def check_ping(address):
  try:
    subprocess.check_output((
      "/usr/bin/ping",
      "-c", "1",
      address,
    ))
    print("ping hit")
    return True
  except Exception:
    print("ping failed")
    return False

def main():
  print("led.py")

  argparser = argparse.ArgumentParser()
  argparser.add_argument("-a", "--address", type=str,
    default="ff:ff:aa:be:4a:1f")
  argparser.add_argument("-k", "--keyword", type=str, default="Triones")
  argparser.add_argument("-s", "--scan", action="store_true")
  args = argparser.parse_args()

  address = args.address
  address_type = bluepy.btle.ADDR_TYPE_PUBLIC

  if args.scan:
    print("scanning for", args.keyword)
    address = None
    address_type = None
    scanner = bluepy.btle.Scanner()
    for scan_result in scanner.scan():
      for adtype, description, value in scan_result.getScanData():
        print("adtype", adtype, "description", description, "value", value)
        if args.keyword in value:
          print("found")
          address = scan_result.addr
          address_type = scan_result.addrType
          break
      if address is not None:
        break

  print("address", address)
  print("address_type", address_type)

  if address:
    # 1.0, 1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 55.0, 89.0, 144.0, 233.0
    led_controller = LEDController(address, address_type,
                                   "0000ffd5-0000-1000-8000-00805f9b34fb",
                                   "0000ffd9-0000-1000-8000-00805f9b34fb",
                                   hsv_min=(0.0, 0.75, 0.25),
                                   hsv_max=(1.0, 1.0, 0.75),
                                   hsv_period=(233.0, 144.0, 89.0),
                                   hsv_offset=(0.0, 0.0, 0.0))
    check_for = "ta-1116"
    last_seen = 0
    on = False
    while True:
      try:
        # Check for device on network
        if (time.time() - last_seen) > 60.0:
          if check_ping(check_for):
            last_seen = time.time()

        # Update LED
        if led_controller.is_connected():
          if (time.time() - last_seen) > 60.0:
            if on:
              print("turning off")
              led_controller.off()
              on = False
            time.sleep(60.0)
          else:
            if not on:
              print("turning on")
              led_controller.on()
              on = True
            print("update")
            led_controller.update()
            time.sleep(60.0 if (led_controller.hsv_period == (0.0, 0.0, 0.0)) else (min(led_controller.hsv_period) / 512.0))
        else:
          led_controller.connect()
      except bluepy.btle.BTLEDisconnectError as exception:
        print("disconnected", exception)
        led_controller.disconnect()
        time.sleep(1.0)

if __name__ == "__main__":
  main()
